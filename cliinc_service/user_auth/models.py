from enum import IntEnum
from django.db import models
from django.contrib.auth.models import AbstractUser


class AccountType(IntEnum):

    client = 1
    executor = 2
    admin = 3


class Account(AbstractUser):
    image = models.ImageField(upload_to='images/', null=True, blank=True)
    address = models.ForeignKey('Address', on_delete=models.CASCADE, null=True)
    type = models.IntegerField(null=False, default=AccountType.client.value)

    def __str__(self) -> str:
        return f'[{self.username}] {self.address.__str__()}'

    @property
    def is_executor(self) -> bool:
        return self.type == AccountType.executor.value

    @property
    def is_client(self) -> bool:
        return self.type == AccountType.client.value


class Address(models.Model):
    city = models.CharField(max_length=64)
    population_centers = models.CharField(max_length=64, null=True, blank=True)
    street = models.CharField(max_length=64)
    house = models.CharField(max_length=64)
    building = models.CharField(max_length=64, null=True, blank=True)
    flat = models.CharField(max_length=64)

    def short(self) -> str:
        return f'{self.street}, {self.house}к{self.building}'

    def __str__(self) -> str:
        return f'{self.city}, ул. {self.street}, д. {self.house} к{self.building}, кв {self.flat}'