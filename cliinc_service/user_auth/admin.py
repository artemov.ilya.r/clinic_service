from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from user_auth.models import Account, Address


@admin.register(Account)
class AccountAdmin(UserAdmin):
    list_display = ('username', 'email', 'type', 'address')
    list_filter = ('type',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email', 'image', 'address')}),
        ('Permissions', {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        ('Important dates', {'fields': ('last_login', 'date_joined')}),
        ('Account type', {'fields': ('type',)}),
    )


@admin.register(Address)
class AddressAdmin(admin.ModelAdmin):
    list_display = ('short', 'city', 'street', 'house', 'building', 'flat')
    search_fields = ('city', 'street', 'house', 'building', 'flat')