from django.conf.urls.static import static
from django.urls import path

from board.views import create, home, order, payment
from cliinc_service import settings

urlpatterns = [
    path('', home),
    path('order/<int:order_id>/', order),
    path('payment/<int:order_id>/', payment),
    path('create', create),
    # path('relation', Relation.as_view()),
    # path('favorites', favorites),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

