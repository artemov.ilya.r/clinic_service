document.addEventListener('DOMContentLoaded', function () {
    // Get the link and the select element
    const link = document.querySelector('a[href="create"]');
    const select = document.querySelector('select');

    // Set the default href to the first selected option's ID
    if (link && select) {
        const firstSelectedOptionId = select.options[select.selectedIndex].id;
        link.href = `create?order_type=${firstSelectedOptionId}`;
    }

    // Add an event listener to the select element
    if (select) {
        select.addEventListener('change', function () {
            // Get the selected option's ID
            const selectedOptionId = this.options[this.selectedIndex].id;

            // Update the link's href attribute
            if (link) {
                link.href = `create?order_type=${selectedOptionId}`;
            }
        });
    }

    const orderFilterSelect = document.getElementById('order-filter');
    orderFilterSelect.addEventListener('change', () => {
        const selectedOption = orderFilterSelect.options[orderFilterSelect.selectedIndex];
        const selectedOrderId = selectedOption.getAttribute('data-id');
        window.location.href = `/?order_type=${selectedOrderId}`;
    });

    const urlParams = new URLSearchParams(window.location.search);
    const currentOrderTypeId = urlParams.get('order_type');
    if (currentOrderTypeId) {
        const options = orderFilterSelect.options;
        for (let i = 0; i < options.length; i++) {
            if (options[i].getAttribute('data-id') === currentOrderTypeId) {
                orderFilterSelect.selectedIndex = i;
                break;
            }
        }
    }
});