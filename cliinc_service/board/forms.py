from __future__ import annotations
from django import forms
from django.db.models import QuerySet

from board.const import RoomType, ClinicType, OrderType, DryCleanMaterial, FurnitureType
from django.utils.translation import gettext_lazy as _
from board.models import BaseOrder, ClinicOrder, DryCleaning, ExternalCleaning, Extra
from cliinc_service.widgets import *


class BaseOrderForm(forms.ModelForm):
    class Meta:
        model = BaseOrder
        fields = ['title', 'text', 'image', 'price', 'deadline', 'extras']
        labels = {
            'title': _('Заголовок'),
            'text': _('Описание'),
            'image': _('Фото'),
            'address': _('Адрес'),
            'price': _('Цена'),
            'deadline': _('Время'),
            'extras': _('Дополнительные услуги'),
        }
        widgets = {
            'title': CustomTextInput,
            'text': CustomTextInput,
            'price': CustomTextInput,
        }

    deadline = forms.DateField(label='Время', required=True, widget=forms.SelectDateWidget)
    extras = forms.ModelMultipleChoiceField(
        label='Дополнительные услуги',
        queryset=Extra.objects.none(),
        widget=forms.CheckboxSelectMultiple,
        required=False
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        order_type = self.instance.order_type() if self.instance else None
        self.fields['extras'].queryset = Extra.objects.filter(type=order_type)

    @staticmethod
    def by_type(order_type: int | OrderType) -> type[BaseOrderForm]:
        if isinstance(order_type, int):
            order_type = OrderType(order_type)
        return {
            OrderType.clinic: ClinicOrderForm,
            OrderType.dry: DryCleaningForm,
            OrderType.external: ExternalCleaningForm,
        }.get(order_type)


class ClinicOrderForm(BaseOrderForm):
    class Meta(BaseOrderForm.Meta):
        model = ClinicOrder
        fields = BaseOrderForm.Meta.fields + ['room_type', 'room_count', 'area', 'type']
        labels = BaseOrderForm.Meta.labels | {
            'room_type': _('Заголовок'),
            'room_count': _('Количество комнат'),
            'area': _('Площадь'),
            'type': _('Тип работ'),
        }
        widgets = BaseOrderForm.Meta.widgets | {
            'room_type': CustomSelect,
            'type': CustomSelect,
            'room_count': CustomTextInput,
            'area': CustomTextInput,
        }

    room_type = forms.ChoiceField(choices=RoomType.as_choice(), widget=CustomSelect)
    type = forms.ChoiceField(choices=ClinicType.as_choice(), widget=CustomSelect)


class DryCleaningForm(BaseOrderForm):
    class Meta(BaseOrderForm.Meta):
        model = DryCleaning
        fields = BaseOrderForm.Meta.fields + ['furniture_type', 'material']
        labels = BaseOrderForm.Meta.labels | {
            'furniture_type': _('Тип мебели'),
            'material': _('Материал'),
        }
        widgets = BaseOrderForm.Meta.widgets | {
            'furniture_type': CustomSelect,
            'material': CustomSelect,
        }

    furniture_type = forms.ChoiceField(label='Тип мебели', choices=FurnitureType.as_choice(), widget=CustomSelect)
    material = forms.ChoiceField(label='Материал', choices=DryCleanMaterial.as_choice(), widget=CustomSelect)


class ExternalCleaningForm(BaseOrderForm):
    class Meta(BaseOrderForm.Meta):
        model = ExternalCleaning
        fields = BaseOrderForm.Meta.fields + ['room_type', 'height', 'area']
        widgets = BaseOrderForm.Meta.widgets | {
            'room_type': CustomTextInput,
            'height': CustomTextInput,
            'area': CustomTextInput,
        }