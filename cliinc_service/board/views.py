from django.shortcuts import render, redirect
from board.forms import BaseOrderForm
from board.models import ClinicOrder, DryCleaning, ExternalCleaning, BaseOrder
from board.const import OrderType, OrderStatus
from user_auth.forms import AddressForm


def home(request):
    order_type = OrderType(int(request.GET.get('order_type'))) if request.GET.get('order_type') else None
    if not order_type:
        order_type = OrderType.clinic
    context = {
        'is_authenticated': request.user.is_authenticated,
    }
    if request.user.is_authenticated:
        context['is_executor'] = request.user.is_executor
        context['is_client'] = request.user.is_client
    if request.user.is_authenticated and request.user.is_client:
        data_list = BaseOrder.by_type(order_type).objects.filter(author=request.user)
    elif request.user.is_authenticated and request.user.is_executor:
        data_list = BaseOrder.by_type(order_type).objects.all()
    else:
        data_list = []
    context |= {
        'orders': data_list,
        'full': False,
        'order_types': OrderType.as_list()
    }
    return render(request, 'board/home.html', context)


def payment(request, order_id: int):
    obj = BaseOrder.find_by_id(order_id)
    context = {
        'order': obj,
    }
    return render(request, 'board/payment.html', context)


def order(request, order_id: int):
    obj = BaseOrder.find_by_id(order_id)
    context = {
        'order': obj,
        'full': True,
        'add_payment': obj.state == OrderStatus.wait,
    }
    return render(request, 'board/order.html', context)


def create(request):
    order_type = OrderType(int(request.GET.get('order_type'))) if request.GET.get('order_type') else None
    if not order_type:
        order_type = OrderType.clinic
    order_form = BaseOrderForm.by_type(order_type)
    if request.method == 'POST':
        form = order_form(request.POST, request.FILES)
        address_form = AddressForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.author = request.user
            obj.save()
            return redirect('/')
        else:
            return render(request, 'layouts/one_form_address.html', {'form': form, 'form_address': address_form})
    filled_address = AddressForm(instance=request.user.address)
    context = {
        'form': order_form(),
        'form_address': filled_address,
        'add_payment': False,
    }
    return render(request, 'layouts/one_form_address.html', context)

#
# def favorites(request):
#     account = request.user
#
#     hidden = Q(
#         accountnoticerelation__account=account,
#         accountnoticerelation__is_in_hidden=True,
#     )
#
#     favorite = Q(
#         accountnoticerelation__account=account,
#         accountnoticerelation__is_in_favorite=True,
#     )
#     notices_set = (
#         ClinicOrder
#         .objects
#         .select_related('author__address')
#         .annotate(
#             is_favorite=Case(
#                 When(
#                     favorite, then=Value(True, output_field=BooleanField(null=True)))
#             ),
#             is_hidden=Case(
#                 When(
#                     hidden, then=Value(True, output_field=BooleanField(null=True)))
#             )
#         ).filter(favorite)
#     )
#     context = {'notices': notices_set}
#     return render(request, 'board/home.html', context)
#
# #
# class Relation(View):
#     class RelationMethods:
#         Hide = 'hide'
#         Favorite = 'favorite'
#
#     def post(self, request):
#         data = json.loads(request.body)
#
#         method = data.get('method')
#         pk = data.get('pk')
#
#         notice = ClinicOrder.objects.get(pk=pk)
#         account = request.user
#
#         relation, _ = AccountClinicOrderRelation.objects.get_or_create(
#             account=account,
#             notice=notice
#         )
#
#         if method == self.RelationMethods.Hide:
#             relation.is_in_hidden = True
#         elif method == self.RelationMethods.Favorite:
#             relation.is_in_favorite = True
#
#         relation.save()
#
#         return HttpResponse('check')
