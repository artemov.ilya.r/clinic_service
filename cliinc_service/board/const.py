from enum import IntEnum


class OrderStatus(IntEnum):
    wait = 0
    paid = 1
    in_progress = 2
    done = 3


class OrderType(IntEnum):
    clinic = 1
    dry = 2
    external = 3

    @classmethod
    def as_list(cls) -> list[dict]:
        return [
            {'title': 'Уборка помещения', 'id': OrderType.clinic.value},
            {'title': 'Химчистка', 'id': OrderType.dry.value},
            {'title': 'Внешние работы', 'id': OrderType.external.value},
        ]


class RoomType(IntEnum):
    private = 1
    commercial = 2

    @classmethod
    def as_choice(cls) -> list[tuple[int, str]]:
        return [(RoomType.private.value, 'Частная собственность'),
                (RoomType.commercial.value, 'Коммерческая недвижимость')]


class ClinicType(IntEnum):
    supporting = 1
    general = 2
    after_rent = 3
    after_repair = 4
    hypoallergenic = 5
    dry = 6

    @classmethod
    def as_choice(cls) -> list[tuple[int, str]]:
        return [
            (cls.supporting.value, 'Поддерживающая'),
            (cls.general.value, 'Генеральная'),
            (cls.after_rent.value, 'После аренды'),
            (cls.after_repair.value, 'После ремонта'),
            (cls.hypoallergenic.value, 'Гипоаллергенная'),
            (cls.dry.value, 'Сухая'),
        ]


class ClinicExtraService(IntEnum):
    trow_trash = 1


class FurnitureType(IntEnum):
    sofa = 1
    wardrobe = 2
    bed = 3
    mattress = 4

    @classmethod
    def as_choice(cls) -> list[tuple[int, str]]:
        return [
            (cls.sofa.value, 'Диван'),
            (cls.wardrobe.value, 'Шкаф'),
            (cls.bed.value, 'Кровать'),
            (cls.mattress.value, 'Матрас'),
        ]


class DryCleanMaterial(IntEnum):
    supporting = 1
    general = 2
    after_rent = 3
    after_repair = 4
    hypoallergenic = 5
    dry = 6

    @classmethod
    def as_choice(cls) -> list[tuple[int, str]]:
        return [
            (cls.supporting.value, 'Текстиль'),
            (cls.general.value, 'Искусственная замша'),
            (cls.after_rent.value, 'Натуральная кожа'),
            (cls.after_repair.value, 'Текстиль'),
            (cls.hypoallergenic.value, 'Другое'),
        ]
