from django.contrib import admin
from board.models import Extra, ClinicOrder, DryCleaning, ExternalCleaning


@admin.register(Extra)
class ExtraAdmin(admin.ModelAdmin):
    list_display = ('type', 'title')
    pass


@admin.register(ClinicOrder)
class ClinicOrderAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'created',
        'address',
        'price',
        'deadline',
        'state',
        'author',
    )
    pass


@admin.register(DryCleaning)
class DryCleaningAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'created',
        'address',
        'price',
        'deadline',
        'state',
        'author',
    )
    pass


@admin.register(ExternalCleaning)
class ExternalCleaningAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'created',
        'address',
        'price',
        'deadline',
        'state',
        'author',
    )
    pass
