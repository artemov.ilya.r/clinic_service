from __future__ import annotations

from django.db import models
from django.db.models import QuerySet
from django.utils import timezone

from board.const import RoomType, OrderType, OrderStatus
from user_auth.models import Account, Address


class Extra(models.Model):
    type = models.IntegerField()
    title = models.CharField(max_length=200)

    def __str__(self) -> str:
        return self.title


class BaseOrder(models.Model):
    class Meta:
        abstract = True

    title = models.CharField(max_length=2000)
    text = models.CharField(max_length=2000)
    image = models.ImageField(upload_to='images/', null=True, blank=True)
    created = models.DateField(auto_now_add=True)
    address = models.ForeignKey(Address, on_delete=models.CASCADE, null=True)
    coord_x = models.FloatField(null=True, default=None)
    coord_y = models.FloatField(null=True, default=None)
    price = models.IntegerField(null=True)
    deadline = models.DateTimeField(default=timezone.now)
    state = models.IntegerField(default=OrderStatus.wait.value)

    extras = models.ManyToManyField(Extra, related_name='%(app_label)s_%(class)s_extras', blank=True)
    author = models.ForeignKey(Account, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_authors')
    executor = models.ForeignKey(Account, null=True, on_delete=models.CASCADE, related_name='%(app_label)s_%(class)s_executors')

    @classmethod
    def order_type(cls) -> int:
        return NotImplemented

    @staticmethod
    def by_type(order_type: int | OrderType) -> type[BaseOrder]:
        if isinstance(order_type, int):
            order_type = OrderType(order_type)
        return {
            OrderType.clinic: ClinicOrder,
            OrderType.dry: DryCleaning,
            OrderType.external: ExternalCleaning,
        }.get(order_type)

    @classmethod
    def find_by_id(cls, id: int) -> BaseOrder:
        return ClinicOrder.objects.get(pk=id) or DryCleaning.objects.get(pk=id) or ExternalCleaning.objects.get(pk=id)

    @classmethod
    def merge_to_base_list(cls, *query_sets: list[QuerySet]) -> list[dict]:
        result = []
        fields = [
            'title', 'text', 'image', 'price', 'deadline',
            'author', 'address', 'created', 'status'
        ]
        for query_set in query_sets:
            result += [
                {field: getattr(row, field) for field in fields}
                for row in query_set
            ]
        return result

    @classmethod
    def get_by_client(cls, user: Account):
        return cls.merge_to_base_list(
            ClinicOrder.objects.filter(author=user),
            DryCleaning.objects.filter(author=user),
            ExternalCleaning.objects.filter(author=user),
        )

    @classmethod
    def get_all(cls) -> list[dict]:
        return cls.merge_to_base_list(
            ClinicOrder.objects.all(),
            DryCleaning.objects.all(),
            ExternalCleaning.objects.all(),
        )


class ClinicOrder(BaseOrder):
    room_type = models.IntegerField(null=False, default=RoomType.private.value)
    room_count = models.IntegerField(null=False, default=1)
    area = models.IntegerField(null=False, default=0)
    type = models.IntegerField(null=False, default=None)

    @classmethod
    def order_type(cls) -> int:
        return OrderType.clinic.value


class DryCleaning(BaseOrder):
    furniture_type = models.IntegerField(null=False, default=None)
    material = models.IntegerField(null=False, default=None)

    @classmethod
    def order_type(cls) -> int:
        return OrderType.dry.value


class ExternalCleaning(BaseOrder):
    room_type = models.IntegerField(null=False, default=RoomType.private.value)
    height = models.IntegerField(null=False, default=200)
    area = models.IntegerField(null=False, default=100)

    @classmethod
    def order_type(cls) -> int:
        return OrderType.external.value
