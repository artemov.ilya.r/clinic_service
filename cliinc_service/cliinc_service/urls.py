from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', include('user_auth.urls')),
    path('', include('board.urls')),
    # path('__debug__/', include('debug_toolbar.urls')),
]