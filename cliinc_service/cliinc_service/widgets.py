from django.forms.widgets import TextInput, Textarea, Select


class CustomTextInput(TextInput):
    input_type = 'text'
    template_name = 'widgets/text_input.html'


class CustomEmailInput(TextInput):
    input_type = "email"
    template_name = 'widgets/text_input.html'


class CustomPasswordInput(TextInput):
    input_type = "password"
    template_name = 'widgets/text_input.html'


class CustomTextArea(Textarea):
    input_type = "password"
    template_name = 'widgets/textarea.html'


class CustomSelect(Select):
    input_type = "select"
    template_name = 'widgets/select.html'
